package org.ximinghui.console.banner;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 牛图案测试用例
 */
class TestBullBannerCase {

    /**
     * 牛图案打印测试
     */
    @Test
    void printCase() {
        List<String> lines = new BullBanner().getBanner();
        assertNotNull(lines);
        lines.forEach(System.out::println);
    }

}
