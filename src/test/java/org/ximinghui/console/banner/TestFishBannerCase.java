package org.ximinghui.console.banner;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * 鱼图案测试用例
 */
class TestFishBannerCase {

    /**
     * 鱼图案打印测试
     */
    @Test
    void printCase() {
        List<String> lines = new FishBanner().getBanner();
        assertNotNull(lines);
        lines.forEach(System.out::println);
    }

}
