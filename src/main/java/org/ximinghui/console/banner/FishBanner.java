package org.ximinghui.console.banner;

import org.ximinghui.common.util.naming.Convention;

/**
 * The fish banner implement
 * 鱼横幅实现示例
 *
 * <p>
 * 图案来源于<a href="https://fontawesome.com/icons/fish">Font Awesome</a>站点
 */
public class FishBanner extends AbstractBanner {

    private static final String LOGO = "                  !!!!!!!!!!!!!!\n" +
            "  !!           !!!!!!!!!!!!!!!!!!!!!\n" +
            "!!!!!!!!    !!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            "   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            "    !!!!!!!!!!!!!!!!!!!!!!!!!    !!!!!!!!!!\n" +
            "  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            " !!!!!!      !!!!!!!!!!!!!!!!!!!!!!!!!\n" +
            "               !!!!!!!!!!!!!!!!!!!!\n" +
            "                   !!!!!!!!!!!!";

    private static final String INFORMATION = "{project-name} {project-version}\n" +
            "\n" +
            "OS: {os-name}\n" +
            "Host: {host}\n" +
            "User: {user}\n" +
            "PID: {pid}\n" +
            "\n" +
            "\n" +
            "https://www.ximinghui.org";

    public FishBanner() {
        this(true);
    }

    public FishBanner(boolean randomPrint) {
        super(LOGO, INFORMATION, randomPrint, Convention.START_CASE);
    }

}
