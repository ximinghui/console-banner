package org.ximinghui.console.banner.util;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.ximinghui.common.util.naming.Convention;
import org.ximinghui.common.util.naming.Converter;

import java.io.FileReader;
import java.io.IOException;

/**
 * Utility for Maven information
 * Maven信息工具类
 */
@Log
public class Maven {

    /**
     * 禁止实例化该工具类
     */
    private Maven() {
    }

    /**
     * Get project object model information
     * 获取项目POM信息
     *
     * @return 项目模型对象
     */
    public static Model getProjectInformation() {
        try {
            return new MavenXpp3Reader().read(new FileReader("pom.xml"));
        } catch (IOException | XmlPullParserException e) {
            log.warning("读取Maven项目信息失败");
            return new Model();
        }
    }

    /**
     * Get artifact id from project model
     * 获取项目工件Id
     *
     * @param model 项目模型对象
     * @return 项目工件Id
     */
    public static String getArtifactId(Model model) {
        return model.getArtifactId();
    }

    /**
     * Get project name from project model
     * 获取项目名
     * <p>
     * 若未定义Maven项目名，则返回工件ID
     *
     * @param model 项目模型对象
     * @return 项目名
     */
    public static String getProjectName(Model model) {
        String name = model.getName();
        if (StringUtils.isNotBlank(name)) return name;
        log.info("无检测到项目名，尝试获取工件ID作为替代");
        return getArtifactId(model);
    }

    /**
     * Get project name formatted with the specified naming convention from the project model
     * 获取格式化项目名
     *
     * @param model  项目模型对象
     * @param format 格式化命名规则
     * @return 格式化项目名
     */
    public static String getProjectName(Model model, Convention format) {
        return formatName(getProjectName(model), format);
    }

    /**
     * Get version from project model
     * 获取项目版本号
     *
     * @param model 项目模型对象
     * @return 项目版本号
     */
    public static String getProjectVersion(Model model) {
        return model.getVersion();
    }

    /**
     * Format project name
     * 格式化项目名
     *
     * @param name   项目名
     * @param format 格式化命名规则
     * @return 格式化后的项目名
     */
    public static String formatName(String name, Convention format) {
        return Converter.convert(name, format);
    }

}
