package org.ximinghui.console.banner;

import org.ximinghui.common.util.naming.Convention;

/**
 * The bull banner implement
 * 牛横幅实现示例
 *
 * <p>
 * 图案来源于<a href="https://icons8.com/icon/1974/bull">Icons8</a>站点
 */
public class BullBanner extends AbstractBanner {

    private static final String LOGO = "     !!!!                            !!!!\n" +
            "   !!!!!                              !!!!!\n" +
            " !!!!!!              !!!!              !!!!!!\n" +
            " !!!!!!          !!!!!!!!!!!!          !!!!!!\n" +
            " !!!  !!!!!!!!!!!!          !!!!!!!!!!!!  !!!\n" +
            "  !!!!      !!!!              !!!!      !!!!\n" +
            "   !!!!!!!!!!!                  !!!!!!!!!!!\n" +
            "!!!!!    !!!!                    !!!!    !!!!!\n" +
            "  !!!!!!!!!                        !!!!!!!!!\n" +
            "       !!!!  !!                !!  !!!!\n" +
            "      !!!!   !!!!!!        !!!!!!   !!!!\n" +
            "       !!!!!  !!!!!!      !!!!!!  !!!!!\n" +
            "          !!!!                  !!!!\n" +
            "            !!!                !!!\n" +
            "             !!!!            !!!!\n" +
            "              !!!            !!!\n" +
            "              !!!!!!!    !!!!!!\n" +
            "               !!!!!!!!!!!!!!!!\n" +
            "                 !!!      !!!\n" +
            "                  !!!!!!!!!!";

    private static final String INFORMATION = "{project-name} {project-version}\n" +
            "\n" +
            "OS: {os-name}\n" +
            "Host: {host}\n" +
            "User: {user}\n" +
            "PID: {pid}\n" +
            "\n" +
            "Java: {java-version}\n" +
            "JVM: {jvm-name}\n" +
            "JDK: {java-home}\n" +
            "JDK build: {jdk-version}\n" +
            "\n" +
            "\n" +
            "https://www.ximinghui.org";

    public BullBanner() {
        super(LOGO, INFORMATION);
    }

    public BullBanner(boolean randomPrint) {
        super(LOGO, INFORMATION, randomPrint);
    }

    public BullBanner(Convention projectNameFormat) {
        super(LOGO, INFORMATION, projectNameFormat);
    }

    public BullBanner(boolean randomPrint, Convention projectNameFormat) {
        super(LOGO, INFORMATION, randomPrint, projectNameFormat);
    }

}
