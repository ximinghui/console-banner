package org.ximinghui.console.banner;

import lombok.Data;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Model;
import org.ximinghui.common.util.*;
import org.ximinghui.common.util.naming.Convention;
import org.ximinghui.console.banner.util.Maven;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Basic banner abstract class
 * 基础的横幅抽象类
 */
@Log
@Data
public abstract class AbstractBanner {

    /**
     * Logo pattern definition
     * Logo图案定义
     */
    private String logo;

    /**
     * Run information definition
     * 运行信息定义
     */
    private String information;

    /**
     * Print a pattern with random characters
     * 随机字符打印图案
     */
    private boolean randomPrint;

    /**
     * Project name formatting
     * 项目名格式化
     */
    private Convention projectNameFormat;

    /**
     * Create a Banner instance
     * 创建横幅实例
     *
     * @param logo        Logo图案块儿
     * @param information 信息块儿
     */
    protected AbstractBanner(String logo, String information) {
        this(logo, information, true);
    }

    /**
     * Create a Banner instance
     * 创建横幅实例
     *
     * @param logo        Logo图案块儿
     * @param information 信息块儿
     * @param randomPrint 随机字符打印
     */
    protected AbstractBanner(String logo, String information, boolean randomPrint) {
        this(logo, information, randomPrint, null);
    }

    /**
     * Create a banner instance printed with random characters and formatted with project name
     * 创建使用随机字符打印且带项目名格式化的横幅实例
     *
     * @param logo              Logo图案块儿
     * @param information       信息块儿
     * @param projectNameFormat 项目名格式化风格
     */
    protected AbstractBanner(String logo, String information, Convention projectNameFormat) {
        this(logo, information, true, projectNameFormat);
    }

    /**
     * Create a banner instance formatted with project name
     * 创建带项目名格式化的横幅实例
     *
     * @param logo              Logo图案块儿
     * @param information       信息块儿
     * @param randomPrint       随机字符打印
     * @param projectNameFormat 项目名格式化风格
     */
    protected AbstractBanner(String logo, String information, boolean randomPrint, Convention projectNameFormat) {
        this.logo = logo;
        this.information = information;
        this.randomPrint = randomPrint;
        this.projectNameFormat = projectNameFormat;
    }

    /**
     * Get banner information
     * 获取横幅信息
     *
     * @return 横幅行数据
     */
    public List<String> getBanner() {
        try {
            return obtainBanner();
        } catch (RuntimeException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Get banner information
     * 获取横幅信息
     *
     * @return 横幅行数据
     */
    protected List<String> obtainBanner() {
        // 将LOGO定义模板转换为行字符串
        List<String> logoLines = getLogo();
        // 将信息定义模板转换为行字符串
        List<String> infoLines = getInformation();

        // 获取LOGO文本行数
        int logoLineNum = logoLines.size();
        // 获取信息文本行数
        int infoLineNum = infoLines.size();
        // 横幅文本行数：取两者行数最多（即最高）
        int bannerLineNum = Math.max(logoLineNum, infoLineNum);

        // LOGO最大字符宽度：获取文本块儿中最宽一行字符串的宽度
        int logoWidth = Containers.maxValue(logoLines, String::length) + 5;
        // 信息最大字符宽度：获取文本块儿中最宽一行字符串的宽度
        int infoWidth = Containers.maxValue(infoLines, String::length);
        // LOGO矩形文本块儿格式化定义：即将不足最大宽度的文本行以空格补充对其
        String logoLineFormat = "%-" + logoWidth + "s";
        // 信息矩形文本块儿格式化定义：即将不足最大宽度的文本行以空格补充对其
        String infoLineFormat = "%-" + infoWidth + "s";

        // 信息文本块儿偏移量：用于左部分和右部分行高不一致时两者垂直居中对齐
        int infoLineOffset = logoLineNum != infoLineNum ? (logoLineNum - infoLineNum) / 2 : 0;
        // 偏移向量：即偏移量的绝对值
        int offsetAbsValue = Math.abs(infoLineOffset);
        // 标志信息文本块儿是否缩进
        boolean infoShouldIndentation = infoLineOffset > 0;

        // 横幅行数据：固定大小List对象
        List<String> bannerLines = Arrays.asList(new String[bannerLineNum]);

        // 遍历填充横幅行数据
        for (int i = 0; i < bannerLineNum; i++) {
            String left;
            String right;
            // 获取左右两部分行内容
            if (i >= offsetAbsValue && infoShouldIndentation) {
                left = obtainSpecifiedLine(logoLines, i);
                right = obtainSpecifiedLine(infoLines, i - offsetAbsValue);
            } else if (i >= offsetAbsValue) {
                left = obtainSpecifiedLine(logoLines, i - offsetAbsValue);
                right = obtainSpecifiedLine(infoLines, i);
            } else if (infoShouldIndentation) {
                left = obtainSpecifiedLine(logoLines, i);
                right = StringUtils.EMPTY;
            } else {
                left = StringUtils.EMPTY;
                right = obtainSpecifiedLine(infoLines, i);
            }
            bannerLines.set(i, String.format(logoLineFormat, left) + String.format(infoLineFormat, right));
        }

        // 返回横幅行数据
        return bannerLines;
    }

    /**
     * 获取Logo图案
     *
     * @return Logo图案
     */
    private List<String> getLogo() {
        if (!randomPrint) return Strings.splitLineAsList(logo);
        String randomChar = Strings.randomVisibleChar(Characters.CharacterSet.US_ASCII);
        return Strings.splitLineAsList(logo.replaceAll("\\S", randomChar));
    }

    /**
     * 获取项目运行信息
     *
     * @return 项目运行信息
     */
    private List<String> getInformation() {
        Model projectModel = Maven.getProjectInformation();
        String projectName = projectNameFormat != null ? Maven.getProjectName(projectModel, projectNameFormat) : Maven.getProjectName(projectModel);
        String projectVersion = Maven.getProjectVersion(projectModel);
        String operatingSystemName = SystemProperty.getOperatingSystemName();
        String hostName = SystemProperty.getHostName().orElse("unknown");
        String userName = SystemProperty.getCurrentUser();
        String javaVersion = SystemProperty.getJavaVersion();
        String javaHome = SystemProperty.getJavaHome();
        String jvmName = SystemProperty.getJVMName();
        Optional<String> maybeJDKVendorVersion = SystemProperty.getJDKVendorVersion();
        String info = this.information;
        info = info.replace("{project-name}", String.valueOf(projectName));
        info = info.replace("{project-version}", String.valueOf(projectVersion));
        info = info.replace("{os-name}", String.valueOf(operatingSystemName));
        info = info.replace("{host}", String.valueOf(hostName));
        info = info.replace("{user}", String.valueOf(userName));
        info = info.replace("{java-version}", String.valueOf(javaVersion));
        info = info.replace("{java-home}", String.valueOf(javaHome));
        info = info.replace("{jvm-name}", String.valueOf(jvmName));
        info = info.replace("{jdk-version}", maybeJDKVendorVersion.orElse("unknown"));
        return Arrays.asList(info.split("\\n"));
    }

    /**
     * 获取指定行文本内容
     *
     * @param lines 字符串行列表
     * @param index 行索引值
     * @return 指定行文本内容
     */
    private String obtainSpecifiedLine(List<String> lines, int index) {
        return Containers.safeObtainElement(lines, index, StringUtils.EMPTY);
    }

}
